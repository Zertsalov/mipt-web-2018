import React from 'react';
import styles from './styles.module.scss';
import { connect } from "react-redux";
import { changeTest } from "actions";

class ListItem extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			checked: false,
		};
	}

	changeChecked = () => {
		this.setState({checked: !this.state.checked});
		this.props.changeComponentTest(this.props.title);
	};

	render() {
		return <div className={this.state.checked ? styles.listItemChecked : styles.listItem}>
			<div onClick={this.changeChecked}>
				{`${this.props.title} ${this.props.priority}`}
			</div>
			<div onClick={this.props.onRemove}>
				Remove
			</div>
		</div>;
	}
}

export default connect(
	state => ({test: state.test}),
	dispatch => ({
		changeComponentTest(text) {
			dispatch(changeTest(text));
		}
	})
)(ListItem)