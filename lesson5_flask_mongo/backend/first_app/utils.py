from bcrypt import gensalt, hashpw, checkpw
from flask_jwt_extended import create_access_token, create_refresh_token, set_access_cookies, set_refresh_cookies


def hash_pass(password):
    password = str.encode(password)
    return hashpw(password, gensalt()).decode()


def check_pass(login_pass, db_pass):
    return checkpw(str.encode(login_pass), str.encode(db_pass))


def add_jwt(resp, user_id):
    # Create the tokens we will be sending back to the user
    access_token = create_access_token(identity=user_id)
    refresh_token = create_refresh_token(identity=user_id)

    # Set the JWT cookies in the response
    set_access_cookies(resp, access_token)
    set_refresh_cookies(resp, refresh_token)
    return resp
