from flask import Flask
from flask_cors import CORS
from flask_mongoengine import MongoEngine
from flask_jwt_extended import JWTManager
from config import Config

app = Flask(__name__)
CORS(app)
app.config.from_object(Config)
db = MongoEngine(app)
jwt = JWTManager(app)

from first_app import routes
from first_app import auth
