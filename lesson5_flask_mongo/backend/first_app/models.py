from datetime import datetime
from first_app import db

from mongoengine import fields


class UserModel(db.Document):
    first_name = fields.StringField(required=True)
    email = fields.EmailField(required=True, unique=True)
    created_at = fields.DateField(default=datetime.now)

    # login = fields.StringField(required=True)
    # password = fields.StringField(required=True)
