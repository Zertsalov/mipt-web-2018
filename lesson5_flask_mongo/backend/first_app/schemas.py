from marshmallow import Schema, fields


class UserSchema(Schema):
    id = fields.Function(lambda obj: str(obj['id']), dump_only=True)
    first_name = fields.String()
    created_at = fields.Date()
    email = fields.Email()

    class Meta:
        strict = True
