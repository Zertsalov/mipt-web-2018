from flask import jsonify
from first_app import app
from webargs.flaskparser import use_args
from marshmallow import fields


@app.route('/hello', methods=['GET'])
@use_args({'name': fields.String(required=True), 'second_name': fields.String(required=True)})
def hello(args):
    return f"Privet, {args['second_name']} {args['name']}!"


# Return validation errors as JSON
@app.errorhandler(422)
@app.errorhandler(400)
def handle_error(err):
    headers = err.data.get("headers", None)
    messages = err.data.get("messages", ["Invalid request."])
    if headers:
        return jsonify({"errors": messages}), err.code, headers
    else:
        return jsonify({"errors": messages}), err.code


