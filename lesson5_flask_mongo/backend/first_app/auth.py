from first_app import app
from flask import jsonify, abort, make_response
from flask_jwt_extended import jwt_required, unset_jwt_cookies, get_jwt_identity
from webargs.flaskparser import use_args

from marshmallow import fields

from mongoengine import DoesNotExist

from first_app.models import UserModel
from first_app.schemas import UserSchema
from first_app.utils import check_pass, add_jwt


@app.route('/login', methods=['POST'])
@use_args({'login': fields.String(required=True), 'password': fields.String(required=True)})
def login(args):
    try:
        user = UserModel.objects.get(login=args['login'])
        if not check_pass(args['password'], user.password):
            abort(make_response('Invalid password', 403))
        return add_jwt(jsonify(UserSchema().dump(user).data), str(user.id))
    except DoesNotExist:
        abort(make_response('This user does not exits', 400))


@app.route('/api/users/logout', methods=['POST'])
@jwt_required
def logout():
    resp = jsonify({'logout': True})
    unset_jwt_cookies(resp)
    return resp, 200


@app.route('/protected', methods=['GET'])
@jwt_required
def protected():
    current_user = get_jwt_identity()
    return jsonify(logged_in_as=current_user), 200
