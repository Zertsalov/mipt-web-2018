from pymongo import MongoClient

uri = "mongodb://127.0.0.1:27017"
client = MongoClient(uri)
print(client.testdb.students.count())
print(client.testdb.students.find_one({}))
client.testdb.students.insert_one({'student_id': 50,
                                   'class_id': 7,
                                   'scores': []})
print(client.testdb.students.count())
cursor = client.testdb.students.find({}).sort('student_id').limit(3)
for student in cursor:
    print(student)
