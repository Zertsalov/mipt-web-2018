function Tree(name, value, children) {
    this.name = name;
    this.value = value;
    this.children = children || [];
}

const root = new Tree('root', 0);

const leaf1 = new Tree('leaf_1', 1);
const leaf2 = new Tree('leaf_2', 2);
root.children.push(leaf1, leaf2);

const leaf3 = new Tree('leaf_3', 3);
const leaf4 = new Tree('leaf_4', 4);
leaf1.children.push(leaf3, leaf4);


function getSum(node) {
    let sum = node.value;
    for (const n of node.children) {
        sum += getSum(n);
    }
    return sum;
}

console.log(getSum(root));