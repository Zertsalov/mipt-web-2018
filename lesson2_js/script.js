let email = document.getElementById('email_field');
let login = document.getElementById('login_field');
let tel = document.getElementById('tel_field');

const saveUser = () => {
	if (login.value.length > 0) {
		let storage = localStorage['mipt-web-2019'];
		let users = [];
		if (!storage || !JSON.parse(storage).users) {
			users = [{
				login: login.value,
				email: email.value,
				tel: tel.value,
			}]
		} else {
			users = [...JSON.parse(storage).users, {
				login: login.value,
				email: email.value,
				tel: tel.value,
			}]
		}

		console.log('users:', users);
		localStorage['mipt-web-2019'] = JSON.stringify({users})
	}

	login.value = '';
	email.value = '';
	tel.value = '';
};

const showUsers = () => {
	let loginsList = document.getElementById('login-list');
	let storage = localStorage['mipt-web-2019'];
	if (storage && JSON.parse(storage).users) {
		let items = JSON.parse(storage).users.map(item => `Login: ${item.login} email: ${item.email}`);
		items.forEach(item => {
			let loginList = document.getElementById('login-list');
			let user = document.createElement('div');
			user.class = 'login-item';
			user.innerText = item;
			loginList.appendChild(user);
		});
		loginsList.style.display = "flex";
	}
};

const showHideError = status => {
	console.log(status);
	let error = document.getElementById('login-error');
	error.style.display = status ? "block" : "none";
};

const validateLogin = () => {
	let storage = localStorage['mipt-web-2019'];
	if (!storage || (!JSON.parse(storage).users) ) {
		showHideError(true);
	} else {
		showHideError(JSON.parse(storage).users.map(item => item.login).includes(login.value));
	}
};

login.oninput = validateLogin;

document.getElementById("save-button").addEventListener("click", function(event){
  event.preventDefault();
  saveUser();
});

document.getElementById("list-button").addEventListener("click", function(event){
  event.preventDefault();
  showUsers();
});